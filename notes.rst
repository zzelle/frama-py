=======
 Notes
=======

Documenter le fait que les types testés dans les docstring doivent
être connus dans le namespace du module.

Mettre le assert du type de retour dans la clause else.

Quel workflow pour l'utilisation sur un projet ?

- solution simple (makefile), ok pour lancer des tests sur le code
  converti.

Comment faire pour garder le mapping entre les n° de ligne de code du
fichier d'origine et du fichier généré ?

- pas grand chose sauf ajouter la ligne d'origine dans un message
  d'assert

Idéalement :
  
- vérifie à la conversion que les types déclarés sont connus,
  
- vérifie à la conversion que la syntaxe de déclaration est correcte,

- faire en sorte que ces validateurs soient aussi un checker pylint.  

Variantes :

- partie appel : comment utiliser cet outil pour un analyser abstrait
  (génération de tests)

- partie stub : faire de mock objects pour les analyseurs (ex. la
  fonction qui parle à un SGBD).


Remarques / questions :

- pas de "raise" dans les docstring = n'importe quelle exception peut
  être levée

- comment dire ; aucune exception ? ':raises: None'

- que faire des générateurs ?

- déclaration de types 'conteneur' (liste, tuple, ...) ou pour les
  fonctions.

- docstring sur les classes...
   
  - méthodes : idem fonction
  
  - déclaration des attributs ?

    - éventuellement les remplacer par des property avec des
      validateurs de type
    
    - accès public ?
    
    - vérification qu'ils sont initialisés dans le constructeur
      (est-ce nécessaire ?)

- variables globales : les typer ?

- se servir des docstrings de typage pour aider le moteur d'inférence
  de pylint

- être capable de détecter des violation de propriétés de type
  déclarées dans les docstring depuis pylint


Autres
======

- compléter les types par des infos supplémentaires (contraintes de
  taille (liste, etc.), domaines de valeurs, autres)

- écrire les contraintes en python.
