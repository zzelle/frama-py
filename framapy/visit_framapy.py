#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Augment astroid syntax tree with type assertions based on function docstrings
"""

from docutils.core import publish_doctree

from astroid import MANAGER
from astroid.builder import AstroidBuilder
from astroid.as_string import to_code
from astroid.node_classes import Return, TryExcept, ExceptHandler, Name, AssName
from astroid.scoped_nodes import Function

from pylint.utils import PyLintASTWalker

#from sphinx.util.docstrings import prepare_docstring
from sphinx.domains.python import PyObject

def parse_type_decl(data):
    """parse type declaration in docstring

    Expects a set of type names or None separated by " or "
    """
    #TODO: handle list(foo) (and set, tuple, ...)
    data = data.strip()
    if data == 'None':
        return ['type(None)']
    if ' ' not in data:
        return [data]
    return [x for name in data.split(' or ') for x in parse_type_decl(name)]

class FramaPyVisitor(object):
    """Augment astroid syntax tree with type assertions based on function
    docstrings

    expects parameters in the form :param <type> <name>: or :type <name>: <type>
    and return type in the form :rtype <type>:
    """
    known_fields = dict((f.name, f) for f in PyObject.doc_field_types)

    def __call__(self, node):
        return node.accept(self)

    def visit_function(self, node):
        """parse function/method docstring and add type-checking assertions"""
        if not node.doc:
            return
        doctree = publish_doctree(node.doc).asdom()
        fields = doctree.getElementsByTagName('field')
        paramtypes = {}
        rtype = None
        exceptions = []
        for field in fields:
            # I am assuming that `getElementsByTagName` only returns one element.
            field_name = field.getElementsByTagName('field_name')[0].firstChild.nodeValue
            split_field_name = field_name.split()
            if split_field_name[0] in self.known_fields['parameter'].names:
                if len(split_field_name) == 3:
                    paramtype, paramname = split_field_name[1:3]
                    assert paramname not in paramtypes
                    paramtypes[paramname] = ', '.join(parse_type_decl(paramtype))
            elif split_field_name[0] in self.known_fields['parameter'].typenames:
                if len(split_field_name) != 2:
                    # don't know what to do with this
                    continue
                paramname = split_field_name[1]
                assert paramname not in paramtypes
                body = field.getElementsByTagName('field_body')[0]
                paramtype = " ".join(c.firstChild.nodeValue for c in body.childNodes)
                paramtypes[paramname] = ', '.join(parse_type_decl(paramtype))
            elif split_field_name[0] in self.known_fields['returntype'].names:
                assert rtype is None
                body = field.getElementsByTagName('field_body')[0]
                rtype = " ".join(c.firstChild.nodeValue for c in body.childNodes)
                rtype = ', '.join(parse_type_decl(rtype))
            elif split_field_name[0] in self.known_fields['exceptions'].names:
                assert len(split_field_name) == 2
                exceptions.append(split_field_name[1])
        self.handle_exceptions(node, exceptions)
        self.handle_parameters(node, paramtypes)
        self.handle_returns(node, rtype)

    @staticmethod
    def handle_exceptions(node, exceptions):
        """Wrap node's body in a try/except block checking that exceptions
        match declared types
        """
        if not exceptions:
            return
        exceptions = ', '.join(exceptions)
        replacement = TryExcept()
        replacement.body = node.body
        replacement.handlers = [ExceptHandler()]
        replacement.handlers[0].type = Name()
        replacement.handlers[0].type.name = 'Exception'
        replacement.handlers[0].name = AssName()
        replacement.handlers[0].name.name = 'exc'
        replacement.handlers[0].body = AstroidBuilder(MANAGER).string_build(
            'assert isinstance(exc, (%s)); raise' % exceptions).body
        node.body = [replacement]

    @staticmethod
    def handle_parameters(node, paramtypes):
        """Prepend node's body with assertions that parameters match their
        declared types
        """
        for param, paramtype in paramtypes.items():
            replacement = 'assert isinstance(%s, (%s))' % (param, paramtype)
            node.body[:0] = AstroidBuilder(MANAGER).string_build(replacement).body

    @staticmethod
    def handle_returns(node, rtype):
        """Add assertions to check that all return statements return the right
        type
        """
        if not rtype:
            return
        for return_node in node.nodes_of_class(Return, skip_klass=Function):
            _, sequence = return_node.parent.locate_child(return_node)
            index = sequence.index(return_node)
            replacement = 'retval = %s; assert isinstance(retval, (%s)); return retval' % (
                to_code(return_node.value), rtype)
            sequence[index:index+1] = AstroidBuilder(MANAGER).string_build(replacement).body
        if not isinstance(node.body[-1], Return):
            replacement = 'assert isinstance(None, (%s))' % rtype
            node.body += AstroidBuilder(MANAGER).string_build(replacement).body


class FramaPyASTWalker(PyLintASTWalker):
    def __init__(self):
        # we don't need no linter
        super(FramaPyASTWalker, self).__init__(None)

if __name__ == '__main__':
    source = """
def f(x, y):
    '''
    :param x:
    :type x: int
    :param int y:
    :rtype: int or None
    :raises ValueError:
    '''
    if x:
        return x + y

def g(x):
    '''
    :type x: tuple or list or set or dict
    :rtype: int
    '''
    return len(x)
"""
    builder = AstroidBuilder()
    walker = FramaPyASTWalker()
    ast = builder.string_build(source)
    walker.add_checker(FramaPyVisitor())
    print 'before:'
    print to_code(ast)
    walker.walk(ast)
    print 'after:'
    print to_code(ast)
