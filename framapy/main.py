#!/usr/bin/python
# -*- coding: utf-8 -*-
"""A simple tool that parse docstrings to find annotations and use
them to generate annotated python code, injecting assertions.

From: 

.. code-block:: python

    def f(x, y):
        '''
        :param x:
        :type x: int
        :param int y:
        :rtype: int or None
        :raises ValueError:
        '''
        if x:
            return x + y

    def g(x):
        '''
        :type x: tuple or list or set or dict
        :rtype: int
        '''
        return len(x)

it will produce something like:

.. code-block:: python

    def f(x, y):
        '''
        :param x:
        :type x: int
        :param int y:
        :rtype: int or None
        :raises ValueError:
        '''
        assert isinstance(x, int)
        assert isinstance(y, int)
        try:
            if x:
                retval = (x) + (y)
                assert isinstance(retval, (int, type(None)))
                return retval
        except Exception, exc:
            assert isinstance(exc, ValueError)
            raise
        assert isinstance(None, (int, type(None)))

    def g(x):
        '''
        :type x: tuple or list or set or dict
        :rtype: int
        '''
        assert isinstance(x, (tuple, list, set, dict))
        retval = len(x)
        assert isinstance(retval, int)
        return retval
"""

import os
from astroid.builder import AstroidBuilder
from astroid.as_string import to_code
from .visit_framapy import FramaPyASTWalker, FramaPyVisitor


def py2py(srcfile, dst):
    builder = AstroidBuilder()
    walker = FramaPyASTWalker()
    if isinstance(srcfile, basestring):
        srcfile = open(srcfile)
    ast = builder.string_build(srcfile.read())
    walker.add_checker(FramaPyVisitor())
    walker.walk(ast)
    if os.path.isdir(dst):
        dstfile = os.path.join(dst, os.path.basename(srcfile.name))
    else:
        dstfile = dst
    with open(dstfile, 'w') as f:
        f.write(to_code(ast))


def run():
    import argparse
    parser = argparse.ArgumentParser(description='Annotate Python files to inject assertions.')
    parser.add_argument('pyfiles', metavar='PYFILE', type=file, nargs='+',
                        help='a Python file to annotate')
    parser.add_argument('out', metavar='OUTDIR', type=str, nargs=1,
                        help='destination dirrectory')
    args = parser.parse_args()
    
    if not os.path.isdir(args.out[0]):
        os.makedirs(args.out[0])

    for fname in args.pyfiles:
        py2py(fname, args.out[0])
    
if __name__ == '__main__':
    run()
